package com.phani.restful.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phani.restful.model.Employee;
import com.phani.restful.service.EmployeeService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@ApiOperation(value = "Save Employee")
	@PostMapping("/save")
	public ResponseEntity<String> saveEmployee(@RequestBody Employee emp) {
		try {
			Double sal = emp.getEsal();
			emp.setHra(sal*12/100.0);
			emp.setTa(sal*3/100.0);
			Integer empId = employeeService.saveEmployee(emp);
			return new ResponseEntity<>("Employee saved sucessfully " + empId, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			ResponseEntity<String> resp=new ResponseEntity<String>("Unable to process save", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}

	@ApiOperation(value = "Fetch all Employees")
	@GetMapping("/all")
	public ResponseEntity<?> findAllEmployees() {
		try {
			List<Employee> empList = employeeService.findAll();
			return new ResponseEntity<List<Employee>>(empList, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ResponseEntity<String> resp=new ResponseEntity<String>("Unable to fetch data", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}

	@ApiOperation(value = "Get Employee By Id")
	@GetMapping("/find/{id}")
	public ResponseEntity<?> getEmployeeById(@PathVariable Integer id) {
		try {
			Employee emp = employeeService.getEmployeeById(id);
			return new ResponseEntity<Employee>(emp, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ResponseEntity<String> resp=new ResponseEntity<String>("Unable to fetch data", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}

	@DeleteMapping("/remove/{id}")
	@ApiOperation(value = "Delete Employee By Id")
	public ResponseEntity<?> deleteEmployeeById(@PathVariable Integer id) {
		try {
			employeeService.deleteEmployeeById(id);
			return new ResponseEntity<String>("Employee deleted successfully ", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ResponseEntity<String> resp=new ResponseEntity<String>("Unable to delete", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}
	
	@ApiOperation(value = "Update Employee details")
	@PutMapping("/updateEmployee")
	public ResponseEntity<?> updateEmployeeInfo(@RequestBody Employee emp) {
		ResponseEntity<String> resp = null;
		try {
			employeeService.updateEmployeeInfo(emp);
			resp = new ResponseEntity<String>("Employee info updated successfully! ", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			resp=new ResponseEntity<String>("Unable to update data", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resp;
	}
	
	@PatchMapping("/emailUpdate/{id}/{email}")
	@ApiOperation(value = "Update Employee By Email")
	public ResponseEntity<?> updateEmployeeById(@PathVariable Integer id, @PathVariable String email) {
		try {
			String msg = null;
			Integer count = employeeService.updateEmployeeMail(id, email);
			if (count > 0)
				msg = "Email Updated!";
			else
				msg = "Updation failed!";
			return new ResponseEntity<String>(msg, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			ResponseEntity<String> resp=new ResponseEntity<String>("Unable to update email", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return null;
	}
}
