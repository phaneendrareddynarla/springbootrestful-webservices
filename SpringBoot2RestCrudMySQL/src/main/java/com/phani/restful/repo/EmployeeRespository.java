package com.phani.restful.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.phani.restful.model.Employee;

public interface EmployeeRespository extends JpaRepository<Employee, Integer> {
	
	@Modifying
	@Query("Update Employee set email=:email where empid=:id")
	 public Integer updateEmployeeMail(Integer id, String email);

}
